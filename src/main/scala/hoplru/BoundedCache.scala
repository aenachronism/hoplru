package hoplru

import scala.collection.mutable

/**
  * Created by aenachronism on 2/12/16.
  */
trait Cache[K,V] {
  def put(key: K, value: V): V
  def get(key: K): Option[V]
  def remove(key: K): Option[V]
  def size: Int
}

/**
  * A cache with a user specified capacity.  Once the capacity limit of the cache is reached, elements are
  * purged in a LRU (least recently used) manner.
  *
  * @param capacity The maximum permissible number of cache elements
  * @tparam K The type of the keys in the cache
  * @tparam V The type of the values in the cache
  */
class BoundedCache[K <: AnyRef with Comparable[K], V](val capacity: Int) extends Cache[K,V] {
  require(capacity > 0)

  val cache = mutable.LinkedHashMap.empty[K, V]

  /**
    * Adds the specified key and value to the cache.
    *
    * @param key   The key to add to cache
    * @param value The value to be associated with the key in the cache
    * @return The value initially passed in to the method.  This is useful primarily for the implementation of the
    *         <code>get</code> method
    */
  override def put(key: K, value: V): V = {
    cache.synchronized {
      if (cache.size == capacity) cache -= cache.head._1

      cache += key → value
    }

    value
  }

  /**
    * Retrieve the value associated with the given key, if any.  Note that this method essentially consists of
    * removing a given key, then putting it back into the cache.  By removing then inserting, we update the insertion
    * order on the underlying linked hash map (i.e. we make this element ineligible for purging )
    *
    * @param key The key to locate in the cache
    * @return <code>Some(value)</code> if the key exists in the cache, <code>None</code> otherwise
    */
  override def get(key: K): Option[V] = cache.synchronized {
    remove(key).map(value ⇒ put(key, value))
  }

  /**
    * Remove the data associated with a given key from the cache
    *
    * @param key
    * @return <code>Some(value)</code> if there was previously associated data with the key in the cache,
    *         <code>None</code> otherwise.
    */
  override def remove(key: K): Option[V] = {
    var result: Option[V] = None

    cache.synchronized {
      result = cache.get(key)
      result foreach { _ ⇒ cache -= key }
    }

    result
  }

  /**
    * This method is principally useful for testing, but YMMV
    *
    * @return A count of the number of elements in the cache
    */
  override def size = cache.size
}
