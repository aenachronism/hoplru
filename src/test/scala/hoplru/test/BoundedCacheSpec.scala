package hoplru.test

import hoplru.BoundedCache
import org.scalatest.{Matchers, FlatSpec}

/**
  * Created by aenachronism on 2/13/16.
  */
class BoundedCacheSpec extends FlatSpec with Matchers {

  "A bounded cache" should "initially start out empty" in {
    val cache = new BoundedCache[String, Int](10)
    cache.size should be (0)
  }

  it should "add elements" in {
    val cache = new BoundedCache[String, Int](10)
    cache.size should be (0)
    cache.put("fee", 1)
    cache.size should be (1)
    cache.put("feye", 2)
    cache.size should be (2)
  }

  it should "retrieve elements" in {
    val size  = 100
    val cache = new BoundedCache[String, Int](size)
    (1 to size ).map { i ⇒ cache.put(i.toString, i) }
    cache.size should be (size)
    (1 to size ).map { i ⇒ cache.get(i.toString) should be (Some(i)) }
  }

  it should "not add more elements than its capacity" in {
    val cache = new BoundedCache[String, Int](1)
    cache.size should be (0)
    cache.put("fee", 1)
    cache.size should be (1)
    cache.put("feye", 2)
    cache.size should be (1)
  }

  it should "evict the oldest elements" in {
    val cache = new BoundedCache[String, Int](2)
    cache.put("fee", 1)
    cache.get("fee") should be (Some(1))
    cache.put("feye", 2)
    cache.get("feye") should be (Some(2))
    cache.put("fo", 3)
    cache.get("fo") should be (Some(3))
    cache.get("fee") should be (None)
    cache.get("feye") should be (Some(2))
  }

  it should "remove elements" in {
    val cache = new BoundedCache[String, Int](2)
    cache.put("fee", 1)
    cache.size should be (1)
    cache.remove("fee")
    cache.size should be (0)
    cache.get("fee") should be (None)
  }
}
