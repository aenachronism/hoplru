## A Simple LRU Bounded Cache Implementation

This repository contains the implementation for a simple cache of a bounded size specified by the user at cache construction time.  Elements are persisted until the cache reaches its capacity.  Once at capacity, additional cache puts will result in elements being evicted in order to provide the requisite space.  Eviction is based on the access times of the elements.  The least recently used (LRU) element will be selected for eviction as necessary. 

### Prerequisites

In order to run the code, you will need to have [sbt](http://www.scala-sbt.org) installed on your system.

#### Quick start : Linux / Mac OSX [quickstart]

    $ ./sbt test

#### Quick start : Windows

    c:\downloads> cd hoplru
    c:\downloads\hoplru> sbt test


### Implementation

The LRU cache eviction strategy brings additional complexity to the relatively straightforward problem of implementing a cache in that it introduces the need to manage timestamps in addition to managing the constituent cache elements.  More specifically, it implies that timestamps need to be stored in a sorted container, and that cache elements have a bidirectional association with their timestamps, that is, one can determine a timestamp from a given cache element, and conversely, given a timestamp, one can retrieve the cache element(s) associated with it.

One possible approach to these requirements entails creating several ancillary data structures and writing a good deal of book-keeping code to ensure that all these structures are properly updated as elements are added and removed from the cache.

An alternative approach, and the one that I've used here, is to rely on a hybrid data structure available in the Scala standard collections library that while maintaining hash map semantics is also insertion order aware, the <code>LinkedHashMap</code>.  Since this is a hashed data structure, we should expect calls to <code>put()</code> and <code>get()</code> to execute in constant time, O(1).  Moreover, since insertion order is preserved, evicting an element consists of selecting the first element of a linked list, yet another O(1) operation.


#### Testing

I've included a simple test suite to exercise the basic functionality of the cache.  Please follow the steps in the [quickstart](quickstart) section to run the tests.
